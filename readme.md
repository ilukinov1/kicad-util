Kicad Util
==========

*Because a grab-bag of functionality doesn't deserve a good name.*

This is my personal Kicad companion utility that does the automation that I need for my use of Kicad,
it started out as a PCB layout cloner, but it also grew a panel stitching feature.

TL;DR
-

* There's a demo video showing the panel operation here: https://dren.dk/kicadutil.html
* The latest build of the jar can be downloaded from: http://dren.dk.gitlab.io/kicad-util/kicadutil.jar
* Install java 8 from: https://adoptopenjdk.net/
* Run the jar using: java -jar kicadutil.jar 


News
-
I just implemented attached tabs and explict hole lines that work like this:

![Setting up a frame](img/panel-frame-setup.png "Setting up a frame")

Which results in a panel like this, natually a real panel should include fiducials and alignment holes:

![Frame in 3d](img/panel-with-frame.png "Frame in 3d")

Note: Manual assembly benefits a great deal from having alignment holes that allow the stencil to be
aligned with the frame, so be sure to put at least two non-plated 4 mm holes in each side of the frame
that are also present in the stencil, that way alignment pins can be used to secure the location of the
stencil.


Command Line UI
-

The utility has a command line UI, deal with it.

Usage: java -jar ku.jar pcb -f=<pcbFile> [COMMAND]
Operates on a .kicad_pcb file
  -f, --file=<pcbFile>   The .kicad_pcb file to work on
Commands:
  anchored-clone  Clones a zone on the PCB that is anchored around a part to
                    all the other anchor parts
                  All anchor components must be pre-placed and one of them must
                    be in the template zone
                  All parts must come from hierachial sub-sheets where the
                    annotations are set to start from the next 100 or 1000
                    series.
                    
  array-clone     Clones a zone on the PCB to an array that's offset from the
                    template by a certain x and y pitch.

                  All parts must come from hierachial sub-sheets where the
                    annotations are set to start from the next 100 or 1000
                    series.
                  
  clone-remove    Removes previously created elements such as vias and segments

                  
  text            Modifies all texts as a bulk operation.
                  
  panel           Creates mouse-bites out of Eco1.User lines where they cross
                    Edge.Cuts


PCB layout cloning
------------------

When using hierarchical sheets in kicad it's really easy to draw modules that can be instantiated
many times in a schematic without any extra effort, but the same is not true for the PCB layout
where you're forced to lay out each of the modules individually.

Until now, that is.

With cloning it's relatively easy to clone a layout of one of the instances of the schematic sheets
and apply the layout to all other instances, so the placement, rotation and tracks of one instance gets
replicated to each of the other instances.

The clone operations all assume that:
* The annotations of parts in the sheets have been made so numbering starts a whole 100 or 1000,
so R100 from one sheet is the same resistor as R200 from the next sheet. 
* You have laid out all the components of one sheet with module positions and tracks,
just the way you like it.
* All the modules and tracks of the laid out sheet are surrounded by zone on the Cmts.User layer.   


There are two different PCB layout clone modes:

array-clone
-----------

This mode clones the template into 2d array of targets.

The pitch is the center distance in the x direction of the clones.

The reference offsets are a bit tricky, it's a comma-separated list of
the difference in references between from the template to each of the targets.

If you have 4 sheets each of them with R100, R200, R300, R400 and you have laid out
the sheet that happened to have R200 and you want the targets laid out in the order:
R200, R300, R400, R100, then the parameter ref parameter would be:
--ref=100,200,-100

This mode makes it easy to lay out a regular array of clones, but it's a bit more
tricky to set up the proper command line.
   
~~~~
Usage: java -jar ku.jar pcb array-clone [-c=<xCount>] -p=<xPitch>
                                        -r=<referenceOffsets> [-ypitch=<yPitch>]
Clones a zone on the PCB to an array that's offset from the template by a
certain x and y pitch.

All parts must come from hierachial sub-sheets where the annotations are set to
start from the next 100 or 1000 series.
  -c, --xcount=<xCount>  The number of clones to make per line, the template itself
                           counts as number 0 on the first line
  -p, --pitch, --xpitch=<xPitch>
                         The x distance between centers from the template part to
                           the first target part and between target parts
  -r, --ref, --reference=<referenceOffsets>
                         The offsets from the template zone module references to
                           each of the target groups of modules
                         Repeat this for each of the groups that have to be placed
      --ypitch=<yPitch>  The y distance between centers between the template part
                           and the first target part and between target parts
~~~~


anchored-clone
-
  
This mode allows the clones to be placed in any way you want, including irregular.
 
In addition to the template layout you also need to place one component from each
sheet where you want it, so if you have laid out the sheet with R200, then you could
place just R100, R300 and R400 where you want them and run:

java -jar ku.jar pcb anchored-clone R100 R300 R400

Which will then clone the layouts surrounding R100, R300 and R400 to match the complete
layout around R200.   
  
  
~~~~  
  Usage: java -jar ku.jar pcb anchored-clone [<anchors>...]
  Clones a zone on the PCB that is anchored around a part to all the other anchor
  parts
  All anchor components must be pre-placed and one of them must be in the
  template zone
  All parts must come from hierachial sub-sheets where the annotations are set to
  start from the next 100 or 1000 series.
        [<anchors>...]   The list of component references to use as anchors for the
                           clone operation
~~~~


Panel
-----

The panel mode allows easy generation of the break-away tabs with mouse-bites.

~~~~
Usage: java -jar ku.jar pcb panel [--fillet=<filletRadius>]
                                  [--hole=<holeDiameter>] [--inset=<holeInset>]
                                  [--pitch=<holePitch>] [--width=<width>]
Creates mouse-bites out of Eco1.User lines where they cross Edge.Cuts

Please RTFM before use: https://gitlab.com/dren.dk/kicad-util
      --fillet=<filletRadius>
                            The radius of the routing bit, a larger fillet will
                              cause a thinner tab for the same width,
                            Default is 1 mm for a 2 mm router
      --hole=<holeDiameter> The diameter of the mouse-bite holes,
                            Default is 0.8 mm
      --inset=<holeInset>   The distance from the edge of the board to the center of
                              the mouse-bite holes,
                            Default is 0.25 mm
      --pitch=<holePitch>   The center distance between mouse-bite holes to attempt
                              hitting,
                            Default is 1.3 mm
      --width=<width>       The distance from the middle of the tab to the place
                              where the routing fillet starts,
                            Default is 2.5 mm, if the fillet radius is 1 mm, then
                              you'll end up with a 3 mm wide tab and
                            5 mm from flat edge to flat edge at the root of the
                              mouse-bites
~~~~

Setting up for panelizing is as simple as:
* Start pcbnew.
* Append boards until you have all the boards you want.
* Arrange the boards to have 3-4 mm spacing.
* Draw lines on the Eco1.User layer that bridge from inside neighboring boards as shown here. 
* To turn off the mouse-bites at the end of a tab place a via at the end point of the Eco1.User line.


![Setup](img/panel-setup.png "Setting up panel")

Run the panel command, to get nicely fitted mousebite-tabs like this:

![Output](img/mouse-bites.png "Mouse bites")


![Output](img/panel-with-tabs.png "Panel output")

