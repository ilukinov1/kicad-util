package dk.dren.kicad.textops;

import dk.dren.kicad.pcb.FpText;
import dk.dren.kicad.pcb.MutableValue;
import dk.dren.kicad.pcb.RawNode;
import org.decimal4j.immutable.Decimal6f;

import java.util.List;

public class SetSize implements TextOperation {
    private final MutableValue xAndY;
    private final MutableValue thickness;

    public SetSize(Decimal6f size) {
        xAndY = new MutableValue(size);
        thickness = new MutableValue(size.divide(6));
    }

    @Override
    public void accept(FpText fpText) {
        RawNode effects = (RawNode) fpText.getProperty("effects");
        RawNode font = (RawNode)effects.getProperty("font");
        List<MutableValue> size = ((RawNode) font.getProperty("size")).getAttributes();
        size.get(0).setValue(xAndY);
        size.get(1).setValue(xAndY);

        List<MutableValue> thicknessAttr = ((RawNode) font.getProperty("thickness")).getAttributes();
        thicknessAttr.get(0).setValue(thickness);
    }
}
