package dk.dren.kicad.cmd;

import dk.dren.kicad.pcb.Board;
import dk.dren.kicad.pcb.KicadPcbSerializer;
import dk.dren.kicad.textops.TextOperations;
import org.decimal4j.immutable.Decimal6f;
import picocli.CommandLine;

import java.io.File;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.Callable;

@CommandLine.Command(name = "text", description = "Modifies all texts as a bulk operation.")
public class TextCmd  implements Callable<Integer> {
    @CommandLine.ParentCommand
    PCBCmd pcbCmd;

    @CommandLine.Option(
            names = {"--move-user"},
            description = "Moves the user text away from the origin of the modules so it's possible to grab the module."
    )
    private boolean moveUser;

    @CommandLine.Option(
            names = {"--hide-user"},
            description = "Hides the user text."
    )
    private boolean hideUser;

    @CommandLine.Option(
            names = {"--show-value"},
            description = "Shows the value text."
    )
    private boolean showValue;

    @CommandLine.Option(
            names = {"--move-ref"},
            description = "Moves the rev text away from the origin of the modules so it's possible to grab the module."
    )
    private boolean moveRef;

    @CommandLine.Option(
            names = {"--zero-user"},
            description = "Moves the user text back to 0,0 for when selecting modules isn't important any more."
    )
    private boolean zeroUser;


    @CommandLine.Option(
            names = {"--ref-layer"},
            description = "Sets the layer of the reference texts"
    )
    private String layerRef;

    @CommandLine.Option(
            names = {"--ref-hide"},
            description = "Hides the reference text, so you don't have to look at it unless you explicitly makes it visible"
    )
    private boolean hideRef;

    @CommandLine.Option(
            names = {"--ref-show"},
            description = "Shows the reference text"
    )
    private boolean showRef;



    @CommandLine.Option(
            names = {"--ref-size"},
            description = "Sets the size of reference text to this height/width in mm and the thickness to size/6"
    )
    private Decimal6f sizeRef;

    @CommandLine.Option(
            names = {"--move-value"},
            description = "Moves the value text away from the origin of the modules so it's possible to grab the module."
    )
    private boolean moveValue;

    @CommandLine.Option(
            names = {"--value-layer"},
            description = "Sets the layer of the value texts"
    )
    private String layerValue;

    @CommandLine.Option(
            names = {"--value-hide"},
            description = "Hides the value texts, so you don't have to look at it unless you explicitly makes it visible"
    )
    private boolean hideValue;

    @CommandLine.Option(
            names = {"--value-size"},
            description = "Sets the size of value text to this height/width in mm and the thickness to size/6"
    )
    private Decimal6f sizeValue;


    @CommandLine.Option(
            names = {"--auto"},
            description = "Alias for --move-user --ref-layer=F.Fab --move-ref --ref-size=0.5 --value-layer=F.Fab --value-hide --value-size=0.5"
    )
    private boolean auto;



    @Override
    public Integer call() throws Exception {
        if (auto) {
            moveUser = true;
            hideUser = true;

            moveRef = true;
            layerValue = "F.Fab";
            showValue = true;
            sizeValue = Decimal6f.valueOf(0.25);

            moveValue = true;
            layerRef="F.Fab";
            showRef = true;
            sizeRef = Decimal6f.valueOf(0.25);
        }

        Map<String, TextOperations> ops = new TreeMap<>();
        ops.put("user",      TextOperations.builder().moveAwayFromOrigin(moveUser,-0.15).hide(hideUser));
        ops.put("reference", TextOperations.builder().moveAwayFromOrigin(moveRef, -0.15).layer(layerRef).hide(hideRef).show(showRef).size(sizeRef));
        ops.put("value",     TextOperations.builder().moveAwayFromOrigin(moveValue,0.15).layer(layerValue).hide(hideValue).show(showValue).size(sizeValue));

        Board board = pcbCmd.getBoard();
        TextOperations.applyOperationsOnAllPossibleTexts(board, ops);

        File backupFile = KicadPcbSerializer.storeWithBackup(board);

        System.out.println("Modified "+board.getFile()+" stored backup in "+backupFile);

        return 0;
    }
}
